package com.rapiddsubhanu.weddingaunty;

/**
 * Created by User on 10/24/2017.
 */

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

public class ItemTwoFragment extends Fragment {

    TabHost tabHost;

    ExpandableRelativeLayout expandableLayout1, expandableLayout2;
    public static ItemTwoFragment newInstance() {
        ItemTwoFragment fragment = new ItemTwoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_item_two, container, false);

        final TabHost host = (TabHost) rootView.findViewById(R.id.tabHost);

        host.setup();

        //Tab 1
        TabHost.TabSpec spec = host.newTabSpec("Tab One");
        spec.setContent(R.id.tab1);
        spec.setIndicator("To Do List");
        host.addTab(spec);
        host.getTabWidget().getChildAt(0).getLayoutParams().width = 10;



        //Tab 2
        spec = host.newTabSpec("Tab Two");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Shortlisted Vendors");
        host.addTab(spec);
        host.getTabWidget().getChildAt(1).getLayoutParams().width = 85;

        Button p1_button = (Button) rootView.findViewById(R.id.vendorsButton1);
        SpannableString text = new SpannableString(Html.fromHtml("Purohit Ramkrishnan ji, <i>(Priest)</i>"));
        // make "Clicks" (characters 0 to 5) Red
        text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 23, 0);
        // make "Here" (characters 6 to 10) Blue
        text.setSpan(new ForegroundColorSpan(Color.parseColor("#ef5690")), 24, 32, 0);
        // shove our styled text into the Button
        p1_button.setText(text, TextView.BufferType.SPANNABLE);

        Button p2_button = (Button) rootView.findViewById(R.id.vendorsButton2);
        SpannableString text2 = new SpannableString(Html.fromHtml("Raj Kumar, <i>(Photographer)</i>"));
        // make "Clicks" (characters 0 to 5) Red
        text2.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 10, 0);
        // make "Here" (characters 6 to 10) Blue
        text2.setSpan(new ForegroundColorSpan(Color.parseColor("#ef5690")), 11, 25, 0);
        // shove our styled text into the Button
        p2_button.setText(text2, TextView.BufferType.SPANNABLE);

        Button p3_button = (Button) rootView.findViewById(R.id.vendorsButton3);
        SpannableString text3 = new SpannableString(Html.fromHtml("Manigandan, <i>(Food services)</i>"));
        // make "Clicks" (characters 0 to 5) Red
        text3.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 11, 0);
        // make "Here" (characters 6 to 10) Blue
        text3.setSpan(new ForegroundColorSpan(Color.parseColor("#ef5690")), 12, 27, 0);
        // shove our styled text into the Button
        p3_button.setText(text3, TextView.BufferType.SPANNABLE);

        Button p4_button = (Button) rootView.findViewById(R.id.vendorsButton4);
        SpannableString text4 = new SpannableString(Html.fromHtml("Kiran kumari, <i>(Hair Stylist)</i>"));
        // make "Clicks" (characters 0 to 5) Red
        text4.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 13, 0);
        // make "Here" (characters 6 to 10) Blue
        text4.setSpan(new ForegroundColorSpan(Color.parseColor("#ef5690")), 14, 28, 0);
        // shove our styled text into the Button
        p4_button.setText(text4, TextView.BufferType.SPANNABLE);

        Button p5_button = (Button) rootView.findViewById(R.id.vendorsButton5);
        SpannableString text5 = new SpannableString(Html.fromHtml("Deepika, <i>(fashion consultant)</i>"));
        // make "Clicks" (characters 0 to 5) Red
        text5.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 8, 0);
        // make "Here" (characters 6 to 10) Blue
        text5.setSpan(new ForegroundColorSpan(Color.parseColor("#ef5690")), 9, 29, 0);
        // shove our styled text into the Button
        p5_button.setText(text5, TextView.BufferType.SPANNABLE);

        Button p6_button = (Button) rootView.findViewById(R.id.vendorsButton6);
        SpannableString text6 = new SpannableString(Html.fromHtml("Kumar, <i>(Taxi service)</i>"));
        // make "Clicks" (characters 0 to 5) Red
        text6.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 6, 0);
        // make "Here" (characters 6 to 10) Blue
        text6.setSpan(new ForegroundColorSpan(Color.parseColor("#ef5690")), 7, 21, 0);
        // shove our styled text into the Button
        p6_button.setText(text6, TextView.BufferType.SPANNABLE);
//
     Button p7_button = (Button) rootView.findViewById(R.id.vendorsButton7);
        SpannableString text7 = new SpannableString(Html.fromHtml("Praveenji, <i>(Hotel)</i>"));
        // make "Clicks" (characters 0 to 5) Red
        text7.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 9, 0);
        // make "Here" (characters 6 to 10) Blue
        text7.setSpan(new ForegroundColorSpan(Color.parseColor("#ef5690")), 10, 18, 0);
        // shove our styled text into the Button
        p7_button.setText(text7, TextView.BufferType.SPANNABLE);

        Button p8_button = (Button) rootView.findViewById(R.id.vendorsButton8);
        SpannableString text8 = new SpannableString(Html.fromHtml("Soniya, <i>(Gagan)</i>"));
        // make "Clicks" (characters 0 to 5) Red
        text8.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 7, 0);
        // make "Here" (characters 6 to 10) Blue
        text8.setSpan(new ForegroundColorSpan(Color.parseColor("#ef5690")), 8, 15, 0);
        // shove our styled text into the Button
        p8_button.setText(text8, TextView.BufferType.SPANNABLE);

        Button p9_button = (Button) rootView.findViewById(R.id.vendorsButton9);
        SpannableString text9 = new SpannableString(Html.fromHtml("Gagan, <i>(Gift)</i>"));
        // make "Clicks" (characters 0 to 5) Red
        text9.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 6, 0);
        // make "Here" (characters 6 to 10) Blue
        text9.setSpan(new ForegroundColorSpan(Color.parseColor("#ef5690")), 7, 13, 0);
        // shove our styled text into the Button
        p9_button.setText(text9, TextView.BufferType.SPANNABLE);

        //Tab 3
        spec = host.newTabSpec("Tab Three");
        spec.setContent(R.id.tab3);
        spec.setIndicator("Personal Notes");
        host.addTab(spec);
        host.getTabWidget().getChildAt(2).getLayoutParams().width = 45;
//        Typeface mistralFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/mistral.ttf");

        TextView tvtxt1 = (TextView) rootView.findViewById(R.id.fittwv1);


        TextView tvtxt2 = (TextView) rootView.findViewById(R.id.fittwv2);


        TextView tvtxt3 = (TextView) rootView.findViewById(R.id.fittwv3);


        TextView tvtxt4 = (TextView) rootView.findViewById(R.id.fittwv4);


        return rootView;
    }


}