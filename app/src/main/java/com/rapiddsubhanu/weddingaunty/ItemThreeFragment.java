package com.rapiddsubhanu.weddingaunty;

/**
 * Created by User on 10/24/2017.
 */

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

public class ItemThreeFragment extends Fragment   {
    ExpandableRelativeLayout expandableLayout1;
    Button expandableButton1;



    public static ItemThreeFragment newInstance() {
        ItemThreeFragment fragment = new ItemThreeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_item_three, container, false);

        expandableLayout1 = (ExpandableRelativeLayout) rootView.findViewById(R.id.expandableLayout1);
        expandableButton1 = (Button) rootView.findViewById(R.id.expandableButton1);

        expandableButton1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                expandableLayout1.toggle();



            }

        });

        Button pbutton = (Button) rootView.findViewById(R.id.expandableButton5);
        pbutton.setText("Bridal & Bridegroom Dress");

        Button p1_button = (Button) rootView.findViewById(R.id.expandableButton5);
        p1_button.setText("Bridal & Bridegroom Dress");

        Button p2_button = (Button) rootView.findViewById(R.id.expandableButton8);
        p2_button.setText("DJ & Dance Troupe");

        Button p3_button = (Button) rootView.findViewById(R.id.expandableButton9);
        p3_button.setText("Ghodi wale (Horse &/ Carriage)");
        return rootView;
    }


//
//    public void onClick(View view) {
//        if (view.getId() == R.id.expandableButton1) {
//            expandableLayout1.toggle(); // toggle expand and collapse
//        } else {
//            expandableLayout1.toggle(); // toggle expand and collapse
//        }
//    }



}