package com.rapiddsubhanu.weddingaunty;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by User on 11/3/2017.
 */

public class OncePlanFullFragment extends Fragment {

    public static OncePlanFullFragment newInstance() {
        OncePlanFullFragment fragment = new OncePlanFullFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.onceplanfull, container, false);

        return rootView;

    }
}