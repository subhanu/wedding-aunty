package com.rapiddsubhanu.weddingaunty;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by User on 11/3/2017.
 */

public class MarriagePlanFullFragment extends Fragment {

    public static MarriagePlanFullFragment newInstance() {
        MarriagePlanFullFragment fragment = new MarriagePlanFullFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.marriageplanfull, container, false);

        return rootView;

    }
}