package com.rapiddsubhanu.weddingaunty;

/**
 * Created by User on 10/24/2017.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

public class MainActivityz extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private Menu menu;
    TextView mainlabeltext, mainlabeltwo;
    Fragment selectedFragment;
    Button tv;
    TextView tv2;
    DrawerLayout drawer;
    ExpandableRelativeLayout mazexpandableLayout1;
    LinearLayout mazexpandableButton1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainz);

        Toolbar toolbarz = (Toolbar) findViewById(R.id.toolbarz);
        setSupportActionBar(toolbarz);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mainlabeltext = (TextView) findViewById(R.id.toolbar_title);


        mainlabeltext.setText("Plans");

        mainlabeltwo = (TextView) findViewById(R.id.actmainztextz);



        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigationz);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                         selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_item1:
                                selectedFragment = ItemOneFragment.newInstance();
                                mainlabeltext.setText("Plans");
                                break;
                            case R.id.action_item2:
                                selectedFragment = ItemTwoFragment.newInstance();
                                mainlabeltext.setText("My Plan");
                                break;
                            case R.id.action_item3:
                                selectedFragment = ItemThreeFragment.newInstance();
                                mainlabeltext.setText("Vendors");
                                break;
                            case R.id.action_item4:
                                selectedFragment = ItemFourFragment.newInstance();
                                mainlabeltext.setText("Rituals");
                                break;

                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layoutz, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });

         drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbarz, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View v = navigationView.getHeaderView(0);

        tv = (Button) findViewById(R.id.actmainimg4);
        tv.setOnClickListener(this);

        mazexpandableLayout1 = (ExpandableRelativeLayout) findViewById(R.id.secodrawer);
        mazexpandableButton1 = (LinearLayout) findViewById(R.id.fdabtus);

        mazexpandableButton1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                mazexpandableLayout1.toggle();



            }

        });


//        TextView tv = (TextView ) v.findViewById(R.id.textView107);
//        tv.setText("User: "+Login.Username.getText());

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layoutz, ItemOneFragment.newInstance());
        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
//        int id = item.getItemId();

//        selectedFragment = null;
//        if (id == R.id.actmainimg4)
//        {
//            Toast.makeText(getApplicationContext(), "This is my Toast message!",
//                    Toast.LENGTH_LONG).show();
//
//        }
//       else if (id == R.id.actmainfftv48) {
//          Toast.makeText(getApplicationContext(), "This is my Toast message!",
//           Toast.LENGTH_LONG).show();
//       }

        //calling the method displayselectedscreen and passing the id of selected menu


//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.frame_layoutz, selectedFragment);
//        transaction.commit();
//        else if (id == R.id.nav_slideshow) {
//            Intent j = new Intent(getApplicationContext(),DepartmentalOpdActivity.class);
//            startActivity(j);
//            finish();
//        } else if (id == R.id.nav_manage) {
//            Intent j = new Intent(getApplicationContext(),DepartmentalIpdActivity.class);
//            startActivity(j);
//            finish();
//        }else if (id == R.id.receipt_manage) {
//            Intent j = new Intent(getApplicationContext(),ReceiptActivity.class);
//            startActivity(j);
//            finish();
//        }else if (id == R.id.payment_manage) {
//            Intent j = new Intent(getApplicationContext(),PaymentActivity.class);
//            startActivity(j);
//            finish();
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_orders_screen, menu);
        return true;
    }

    @Override
    public void onClick(View v) {

        selectedFragment = null;
        switch (v.getId()) {
            case R.id.actmainimg4:

                selectedFragment = AboutUsFragment.newInstance();
                mainlabeltext.setText("About Us");
                drawer.closeDrawer(Gravity.LEFT);
                break;


        }
        FragmentTransaction transactionz = getSupportFragmentManager().beginTransaction();
        transactionz.replace(R.id.frame_layoutz, selectedFragment);
        transactionz.commit();
    }

    @Override
    public void onBackPressed() {


        new AlertDialog.Builder(this).setMessage("Do you want to Exit ?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        moveTaskToBack(true);
                    }
                }
        ).setNegativeButton("No",null).show();

    }
}