package com.rapiddsubhanu.weddingaunty;

/**
 * Created by User on 10/24/2017.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

public class ItemOneFragment extends Fragment implements View.OnClickListener {

    LinearLayout linearlytone, linearlyttwo, linearlytthree;
    Fragment selectedFragment;
    public static ItemOneFragment newInstance() {
        ItemOneFragment fragment = new ItemOneFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.act_quickplan, container, false);


        linearlytone = (LinearLayout) rootView.findViewById(R.id.actqpllone);
        linearlytone.setOnClickListener(this);

        linearlyttwo = (LinearLayout) rootView.findViewById(R.id.actqplltwo);
        linearlyttwo.setOnClickListener(this);

        linearlytthree = (LinearLayout) rootView.findViewById(R.id.actqpllthree);
        linearlytthree.setOnClickListener(this);


        return rootView;


    }

    @Override
    public void onClick(View v) {

        selectedFragment = null;
        switch (v.getId()) {
            case R.id.actqpllone:
                selectedFragment = QuickPlanFullFragment.newInstance();
                break;
            case R.id.actqplltwo:
                selectedFragment = MarriagePlanFullFragment.newInstance();
                break;
            case R.id.actqpllthree:
                selectedFragment = OncePlanFullFragment.newInstance();
                break;
        }
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layoutz, selectedFragment);
        transaction.commit();


    }


}