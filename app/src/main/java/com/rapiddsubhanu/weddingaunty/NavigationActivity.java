package com.rapiddsubhanu.weddingaunty;

/**
 * Created by User on 10/23/2017.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.TextView;
import android.widget.Toast;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.action_item1:
                                //redirect to another intent
                                Intent b =new Intent(getBaseContext(),NavigationActivity.class);
                                startActivity(b);
                                finish();

                                break;

                            case R.id.action_item2:
                                //redirect to another intent
                                Intent a =new Intent(getBaseContext(),NavigationActivity.class);
                                startActivity(a);
                                finish();

                                break;



                            case R.id.action_item3:
                                //redirect to another intent
                                Intent c =new Intent(getBaseContext(),NavigationActivity.class);
                                startActivity(c);
                                finish();
                                break;

                            case R.id.action_item4:
                                //redirect to another intent
                                Intent d =new Intent(getBaseContext(),NavigationActivity.class);
                                startActivity(d);
                                finish();
                                break;

                        }

                        return true;
                    }
                });



        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View v = navigationView.getHeaderView(0);
//        TextView tv = (TextView ) v.findViewById(R.id.textView107);
//        tv.setText("User: "+Login.Username.getText());


    }




    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */


    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.action_example || item.getItemId() == R.id.action_logoff) {
//            db.LogoutCurrentUser(Login.CollegeId,Login.BranchId,Login.Username.getText().toString().toUpperCase().trim());
//            Intent j = new Intent(getApplicationContext(),Login.class);
//            startActivity(j);
//            finish();
//            return true;
//        }

        return super.onOptionsItemSelected(item);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.nav_camera) {
//            Intent j = new Intent(getApplicationContext(),CentralOpdActivity.class);
//            startActivity(j);
//        } else if (id == R.id.nav_gallery) {
//            Intent j = new Intent(getApplicationContext(),CentralIpdActivity.class);
//            startActivity(j);
//            finish();
//        } else if (id == R.id.nav_slideshow) {
//            Intent j = new Intent(getApplicationContext(),DepartmentalOpdActivity.class);
//            startActivity(j);
//            finish();
//        } else if (id == R.id.nav_manage) {
//            Intent j = new Intent(getApplicationContext(),DepartmentalIpdActivity.class);
//            startActivity(j);
//            finish();
//        }else if (id == R.id.receipt_manage) {
//            Intent j = new Intent(getApplicationContext(),ReceiptActivity.class);
//            startActivity(j);
//            finish();
//        }else if (id == R.id.payment_manage) {
//            Intent j = new Intent(getApplicationContext(),PaymentActivity.class);
//            startActivity(j);
//            finish();
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {


        new AlertDialog.Builder(this).setMessage("Do you want to Exit ?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        moveTaskToBack(true);
                    }
                }
        ).setNegativeButton("No",null).show();

    }
}

